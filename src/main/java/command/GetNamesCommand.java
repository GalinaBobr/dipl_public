package command;

import entity.Part;
import manager.ConfigurationManager;
import pool.ConnectionPool;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GetNamesCommand implements ActionCommand {
    public String execute(HttpServletRequest request) {

        String page = ConfigurationManager.getProperty("path.page.main");
        HttpSession session = request.getSession(true);
        Connection conn = null;
        PreparedStatement st = null;
        List<Part> names = new ArrayList<Part>();
        try {
            conn = ConnectionPool.CONNECTION_POOL.retrieve();
            st = conn.prepareStatement("SELECT id, name FROM parts");
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                names.add(new Part(id, name));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                st.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            ConnectionPool.CONNECTION_POOL.putback(conn);
        }
        session.setAttribute("parts", names);
        return page;
    }
}
