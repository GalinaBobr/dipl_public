package command;

import entity.Part;
import entity.Storage;
import manager.ConfigurationManager;
import pool.ConnectionPool;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AddPartCommand implements ActionCommand {

    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.main");
        HttpSession session = request.getSession(true);
        String idPart = request.getParameter("nameList");
        Connection conn = null;
        PreparedStatement st = null;
        try {
            conn = ConnectionPool.CONNECTION_POOL.retrieve();
            st = conn.prepareStatement("SELECT parts.name FROM parts WHERE parts.id =" + idPart);
            ResultSet resultSet = st.executeQuery();
            resultSet.next();
            Storage.addName(new Part(Integer.parseInt(idPart), resultSet.getString("name")));

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                st.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            ConnectionPool.CONNECTION_POOL.putback(conn);
        }
        session.setAttribute("output", Storage.getListOfNames());
        return page;
    }
}
