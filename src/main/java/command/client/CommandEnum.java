package command.client;

import command.ActionCommand;
import command.AddPartCommand;
import command.ClearSceneCommand;
import command.GetNamesCommand;

/**
 * Storage command.
 */
public enum CommandEnum {
    GETNAMES {
        {
            this.command = new GetNamesCommand();
        }
    },
    ADDPART {
        {
            this.command = new AddPartCommand();
        }
    },
    CLEARSCENE {
        {
            this.command = new ClearSceneCommand();
        }
    };
    ActionCommand command;
    public ActionCommand getCurrentCommand() {
        return command;
    }
}