package calculation;

import java.util.HashMap;
import java.util.Map;

public class ReferenceTable {

    /**
     * Коэффициент для согласования различных размерностей (часы и секунды, метры и сантиметры)
     */
    public static final double MATCHING_COEFFICIENT = 2.778;

    /**
     * Объемная теплоемкость воздуха
     */
    public static final double VOLUMETRIC_AIR_HEAT = 0.336;

    /**
     * Рекомендуемые диаметры круглого воздуховода
     */
    public static final double[] DIAMETERS = {100, 110, 125, 140, 160, 180, 200, 225, 250, 300, 315, 350, 400};

    public static Map<Double, String> AIR_DISTRIBUTORS_DIM = createMap();

    private static Map<Double, String> createMap() {
        Map<Double,String> map = new HashMap<>();
        map.put(180.0, "200x100 мм, 150x150 мм");
        map.put(240.0, "300x100 мм");
        map.put(300.0, "400x100 мм, 200x200 мм");
        map.put(370.0, "500x100 мм, 300x150 мм");
        map.put(420.0, "600x100 мм, 400x150 мм");
        map.put(530.0, "500x150 мм");
        map.put(600.0, "600x150 мм");
        map.put(700.0, "700x150 мм");
        map.put(740.0, "800x150 мм");
        return map;
    }



}
