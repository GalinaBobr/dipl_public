package manager;

import java.util.ResourceBundle;

/**
 * Utility class that retrieves the necessary information from the file "messages.properties".
 */
public class MessageManager {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("messages");
    // класс извлекает информацию из файла messages.properties
    private MessageManager() { }
    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}