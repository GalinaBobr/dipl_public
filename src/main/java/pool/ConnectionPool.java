package pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Vector;

public class ConnectionPool {
    private Vector<Connection> availableConns = new Vector<Connection>();
    private Vector<Connection> usedConns = new Vector<Connection>();
    private String url;

    public ConnectionPool(String url, String driver, int initConnCnt) {
        try {
            Class.forName(driver);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.url = url;
        Connection conn = null;
        for (int i = 0; i < initConnCnt; i++) {
            conn = getConnection(conn);
            if (conn != null) { availableConns.addElement(conn); }
            else {
                break;
            }
        }
    }

    private Connection getConnection(Connection conn) {
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

    public synchronized Connection retrieve() throws SQLException {
        Connection newConn = null;
        if (availableConns.size() == 0) {
            newConn = getConnection(newConn);
        } else {
            newConn = (Connection) availableConns.lastElement();
            availableConns.removeElement(newConn);
        }
        usedConns.addElement(newConn);
        return newConn;
    }

    public synchronized void putback(Connection c) throws NullPointerException {
        if (c != null) {
            if (usedConns.removeElement(c)) {
                availableConns.addElement(c);
            } else {
            }
        }
    }

    public int getAvailableConnsCnt() {
        return availableConns.size();
    }

    public static final ConnectionPool CONNECTION_POOL = new ConnectionPool(
            "jdbc:mysql://localhost:3306/partsofthefan?user=root&password=1",
            "com.mysql.jdbc.Driver",
            10);
}
