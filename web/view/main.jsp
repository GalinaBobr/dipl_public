<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: P.A
  Date: 10-Dec-16
  Time: 16:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<%@ page session="true" import="java.util.*" %>--%>
<html>
<head>
    <title>Constructor</title>
    <script src="js/extLib/three.min.71.js"></script>
    <script src="js/STLLoader.js"></script>
    <script src="js/extLib/THREEx.WindowResize.js"></script>
    <script src="js/extLib/OrbitControls.js"></script>
    <script src="js/script.js"></script>
</head>
<body>
<form name="addPartForm" method="GET" action="/controller">
<select name="nameList">
    <option value="">Choose the part</option>
    <c:forEach items="${parts}" var="part">
        <option value=${part.getId()}>${part.getName()}</option>
    </c:forEach>
</select>
    <input type="hidden" name="command" value="addpart"/>
    <input type="submit" value="Add"/>
</form>
<form name="removeAllPartsForm" method="POST" action="/controller">
    <input type="hidden" name="command" value="clearscene"/>
    <input type="submit" value="Clear scene"/>
</form>
<hr>
<script>

    <c:forEach items="${output}" var="part">
        loadingSTL.loadModels('models/elements/${part.getName()}.stl');
    </c:forEach>
</script>
</body>
</html>
