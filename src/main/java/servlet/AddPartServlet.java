package servlet;

import pool.ConnectionPool;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;

//@WebServlet("/GetUserServlet")
public class AddPartServlet extends HttpServlet {
    private ServletContext context;

    @Override
    public void init(ServletConfig config) throws ServletException {
        this.context = config.getServletContext();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(true);
        String idPart = req.getParameter("nameList");
        if(idPart == null) { return; }
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        Blob element = null;
        PreparedStatement st = null;
        String query = "select stl from parts where id = '" + idPart + "'";
        ServletOutputStream out = resp.getOutputStream();
        try {
            conn = ConnectionPool.CONNECTION_POOL.retrieve();
            st = conn.prepareStatement(query);
            ResultSet resultSet = st.executeQuery();
            while (resultSet.next()) {
                element = resultSet.getBlob("stl");
            }
            resp.setContentType("application/sla");
            //out.write(element,0,element.length());
            InputStream in = element.getBinaryStream();
            int length = (int) element.length();

            int bufferSize = 1024;
            byte[] buffer = new byte[bufferSize];

            while ((length = in.read(buffer)) != -1) {
                out.write(buffer, 0, length);
            }
            in.close();
            out.flush();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                st.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            ConnectionPool.CONNECTION_POOL.putback(conn);
        }
    }

//    @Override
//    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        HttpSession session = req.getSession(true);
//        String idPart = req.getParameter("nameList");
//        if(idPart == null) { return; }
//        Connection conn = null;
//        Statement stmt = null;
//        ResultSet rs = null;
//        Blob element = null;
//        PreparedStatement st = null;
//        String query = "select stl from parts where id = '" + idPart + "'";
//        ServletOutputStream out = resp.getOutputStream();
//        try {
//            conn = ConnectionPool.CONNECTION_POOL.retrieve();
//            st = conn.prepareStatement(query);
//            ResultSet resultSet = st.executeQuery();
//            while (resultSet.next()) {
//                element = resultSet.getBlob("stl");
//            }
//            resp.setContentType("application/sla");
//            //out.write(element,0,element.length());
//            InputStream in = element.getBinaryStream();
//            int length = (int) element.length();
//
//            int bufferSize = 1024;
//            byte[] buffer = new byte[bufferSize];
//
//            while ((length = in.read(buffer)) != -1) {
//                out.write(buffer, 0, length);
//            }
//            in.close();
//            out.flush();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                st.close();
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//            ConnectionPool.CONNECTION_POOL.putback(conn);
//        }
//    }
}
