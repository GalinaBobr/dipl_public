package entity;

import java.util.ArrayList;
import java.util.List;

public class Storage {

    private static List<Part> parts = new ArrayList<Part>();
//    private static Storage instance;
//
//    public static Storage getInstance() {
//        if (instance == null) {
//            instance = new Storage();
//        }
//        return instance;
//    }

    public static void addName(Part part) {
        parts.add(part);
    }

    public static List<Part> getListOfNames() {
        return parts;
    }

    public static void clearList() {parts.clear();}
}
