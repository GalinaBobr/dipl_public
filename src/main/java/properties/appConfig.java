package properties;

import java.util.ResourceBundle;

/**
 * Created by P.A on 09-Dec-15.
 */
public class appConfig {

    private ResourceBundle resourceBundle;
    private static appConfig SINGLETON;

    private appConfig() {
        resourceBundle = ResourceBundle.getBundle("mySql");
    }

    static {
        SINGLETON = new appConfig();
    }

    public static String get(String key) {
        return SINGLETON.resourceBundle.getString(key);
    }

}
