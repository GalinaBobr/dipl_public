package command;

import calculation.VentilationSystem;
import manager.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

public class CalculateVentCommand implements ActionCommand {
    private static final String PARAM_NAME_QTY = "personQty";
    private static final String PARAM_NAME_SQUARE = "roomSquare";
    private static final String PARAM_NAME_HEIGHT = "roomHeight";

    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.calculation");
        int personQty = Integer.parseInt(request.getParameter(PARAM_NAME_QTY));
        double square = Double.parseDouble(request.getParameter(PARAM_NAME_SQUARE));
        double height = Double.parseDouble(request.getParameter(PARAM_NAME_HEIGHT));

        VentilationSystem ventSystem = new VentilationSystem();
        request.setAttribute("airCapacity", ventSystem.getAirCapacity(personQty, square, height));
        request.setAttribute("ductSize", ventSystem.getDuctSize());
        request.setAttribute("recDiam", ventSystem.getRecommendedDiameterByDuctArea());
        request.setAttribute("dimAirDistr", ventSystem.getRecommendedDimensionsOfAirDistributor());
        request.setAttribute("heaterPower", ventSystem.getAirHeaterPower());

        request.setAttribute("amperage", ventSystem.getMaxAmperage());
        String supplyVoltage = (ventSystem.defineSupplyVoltage() == 660) ? "3 фазы" : "220 B";
        request.setAttribute("voltage", supplyVoltage);
        request.setAttribute("energy", ventSystem.getEnergyConsumption());

        return page;
    }
}
