<%--
  Created by IntelliJ IDEA.
  User: P.A
  Date: 02-Apr-17
  Time: 23:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Calculations</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="../../css/bootstrap.css" rel='stylesheet' type='text/css'/>
    <link href="../../css/style2.css" rel='stylesheet' type='text/css'/>
    <link href='//fonts.googleapis.com/css?family=Quicksand:300,400,700' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Exo+2:100,200,300,400,500,600,700,800,900' rel='stylesheet'
          type='text/css'>
    <link rel="stylesheet" href="../fonts/css/font-awesome.min.css">
</head>
<body>
<div class="services_box1">
    <div class="container">
        <form name="newTestForm" method="POST" action="/controller">
            <div class="col-md-6 contact-grid">
                <p style="color:steelblue;" class="your-para"> Ввод исходных данных</p>
                <hr>
                <p class="your-para">Введите количество человек</p>
                <input type="text" value="" name="personQty">
                <p class="your-para">Введите площадь помещения</p>
                <input type="text" value="" name="roomSquare">
                <p class="your-para">Введите высоту помещения</p>
                <input type="text" value="" name="roomHeight">
                <hr>

                <hr>
                <div class="send">
                    <input type="hidden" name="command" value="calculatevent"/>
                    <input type="submit" value="Вычислить">
                </div>
            </div>
        </form>
    </div>
    <hr>
    <hr>
    <p style="color:steelblue;" class="your-para"> Результат расчета общих параметров </p>
    <table>
        <tr>
            <td width="30%">
                Расход воздуха
            </td>
            <td>
                ${airCapacity} м³/ч
            </td>
        </tr>
        <tr>
            <td width="30%">
                Площадь сечения воздуховода
            </td>
            <td>
                ${ductSize} см²
            </td>
        </tr>
        <tr>
            <td width="30%">
                Рекомендуемый диаметр воздуховода
            </td>
            <td>
                Ø ${recDiam} мм
            </td>
        </tr>
        <tr>
            <td width="30%">
                Рекомендуемые размеры решетки
            </td>
            <td>
                ${dimAirDistr}
            </td>
        </tr>
        <tr>
            <td width="30%">
                Мощность калорифера
            </td>
            <td>
                ${heaterPower} кВт
            </td>
        </tr>
    </table>
    <p style="color:steelblue;" class="your-para"> Расчеты для элетрического калорифера </p>
    <table>
        <tr>
            <td width="30%">
                Напряжение питания
            </td>
            <td>
                ${voltage}
            </td>
        </tr>
        <tr>
            <td width="30%">
                Максимальный ток, потребляемый калорифером
            </td>
            <td>
                ${amperage} А
            </td>
        </tr>
        <tr>
            <td width="30%">
                Потребляемая элетроэнергия за март-май 2017
            </td>
            <td>
                ${energy} BYN
            </td>
        </tr>
    </table>
</div>
</body>
</html>
