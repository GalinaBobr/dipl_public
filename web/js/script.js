/**
 *
 * WebGL With Three.js - loading models
 */

// shaders
sbVertexShader = [
    "varying vec3 vWorldPosition;",
    "void main() {",
    "  vec4 worldPosition = modelMatrix * vec4( position, 1.0 );",
    "  vWorldPosition = worldPosition.xyz;",
    "  gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",
    "}",
].join("\n");
sbFragmentShader = [
    "uniform vec3 topColor;",
    "uniform vec3 bottomColor;",
    "uniform float offset;",
    "uniform float exponent;",
    "varying vec3 vWorldPosition;",
    "void main() {",
    "  float h = normalize( vWorldPosition + offset ).y;",
    "  gl_FragColor = vec4( mix( bottomColor, topColor, max( pow( h, exponent ), 0.0 ) ), 1.0 );",
    "}",
].join("\n");

var loadingSTL = {
    scene: null,
    camera: null,
    renderer: null,
    container: null,
    controls: null,
    clock: null,
    plane: null,
    offset: new THREE.Vector3(),
    raycaster: new THREE.Raycaster(),
    objects: [],

    init: function () { // Initialization

        // create main scene
        this.scene = new THREE.Scene();
        this.scene.fog = new THREE.FogExp2(0xcce0ff, 0.0003);

        var SCREEN_WIDTH = window.innerWidth,
            SCREEN_HEIGHT = window.innerHeight;

        // prepare camera
        var VIEW_ANGLE = 60, ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT, NEAR = 0.1, FAR = 20000;
        this.camera = new THREE.PerspectiveCamera(VIEW_ANGLE, ASPECT, NEAR, FAR);
        this.scene.add(this.camera);
        this.camera.position.set(150, 100, -150);
        this.camera.lookAt(new THREE.Vector3(0, 0, 0));

        // prepare renderer
        this.renderer = new THREE.WebGLRenderer({antialias: true});
        this.renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
        this.renderer.setClearColor(this.scene.fog.color);
        this.renderer.shadowMapEnabled = true;
        this.renderer.shadowMapSoft = true;

        // prepare container
        this.container = document.createElement('div');
        document.body.appendChild(this.container);
        this.container.appendChild(this.renderer.domElement);

        // events
        THREEx.WindowResize(this.renderer, this.camera);
        document.addEventListener('mousedown', this.onDocumentMouseDown, false);
        document.addEventListener('mousemove', this.onDocumentMouseMove, false);
        document.addEventListener('mouseup', this.onDocumentMouseUp, false);

        // prepare controls (OrbitControls)
        this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);
        this.controls.target = new THREE.Vector3(0, 0, 0);
        this.controls.maxDistance = 3000;

        // prepare clock
        this.clock = new THREE.Clock();

        // light
        this.scene.add(new THREE.AmbientLight(0x606060));
        var dirLight = new THREE.DirectionalLight(0xffffff);
        dirLight.position.set(200, 200, 1000).normalize();
        this.camera.add(dirLight);
        this.camera.add(dirLight.target);

        // Display skybox
        this.addSkybox();

        // Plane, that helps to determinate an intersection position
        this.plane = new THREE.Mesh(new THREE.PlaneBufferGeometry(500, 500, 8, 8), new THREE.MeshBasicMaterial({color: 0xffffff}));
        this.plane.visible = false;
        this.scene.add(this.plane);
    },
    addSkybox: function () {
        var iSBrsize = 500;
        var uniforms = {
            topColor: {type: "c", value: new THREE.Color(0x0077ff)},
            bottomColor: {type: "c", value: new THREE.Color(0xffffff)},
            offset: {type: "f", value: iSBrsize},
            exponent: {type: "f", value: 1.5}
        }
        var skyGeo = new THREE.SphereGeometry(iSBrsize, 32, 32);
        skyMat = new THREE.ShaderMaterial({
            vertexShader: sbVertexShader,
            fragmentShader: sbFragmentShader,
            uniforms: uniforms,
            side: THREE.DoubleSide,
            fog: false
        });
        skyMesh = new THREE.Mesh(skyGeo, skyMat);
        this.scene.add(skyMesh);
    },
    onDocumentMouseDown: function (event) {
        // Get mouse position
        var mouseX = (event.clientX / window.innerWidth) * 2 - 1;
        var mouseY = -(event.clientY / window.innerHeight) * 2 + 1;
        // Get 3D vector from 3D mouse position using 'unproject' function
        var vector = new THREE.Vector3(mouseX, mouseY, 1);
        vector.unproject(loadingSTL.camera);
        // Set the raycaster position
        loadingSTL.raycaster.set(loadingSTL.camera.position, vector.sub(loadingSTL.camera.position).normalize());
        // Find all intersected objects
        var intersects = loadingSTL.raycaster.intersectObjects(loadingSTL.objects);
        if (intersects.length > 0) {
            // Disable the controls
            loadingSTL.controls.enabled = false;
            // Set the selection - first intersected object
            loadingSTL.selection = intersects[0].object;
            // Calculate the offset
            var intersects = loadingSTL.raycaster.intersectObject(loadingSTL.plane);
            loadingSTL.offset.copy(intersects[0].point).sub(loadingSTL.plane.position);
        }
    },
    onDocumentMouseMove: function (event) {
        event.preventDefault();
        // Get mouse position
        var mouseX = (event.clientX / window.innerWidth) * 2 - 1;
        var mouseY = -(event.clientY / window.innerHeight) * 2 + 1;
        // Get 3D vector from 3D mouse position using 'unproject' function
        var vector = new THREE.Vector3(mouseX, mouseY, 1);
        vector.unproject(loadingSTL.camera);
        // Set the raycaster position
        loadingSTL.raycaster.set(loadingSTL.camera.position, vector.sub(loadingSTL.camera.position).normalize());
        if (loadingSTL.selection) {
            // Check the position where the plane is intersected
            var intersects = loadingSTL.raycaster.intersectObject(loadingSTL.plane);
            // Reposition the object based on the intersection point with the plane
            loadingSTL.selection.position.copy(intersects[0].point.sub(loadingSTL.offset));
        } else {
            // Update position of the plane if need
            var intersects = loadingSTL.raycaster.intersectObjects(loadingSTL.objects);
            if (intersects.length > 0) {
                loadingSTL.plane.position.copy(intersects[0].object.position);
                loadingSTL.plane.lookAt(loadingSTL.camera.position);
            }
        }
    },
    onDocumentMouseUp: function (event) {
        // Enable the controls
        loadingSTL.controls.enabled = true;
        loadingSTL.selection = null;
    },
    loadModels: function (name) {
        // prepare STL loader and load the model
        var oStlLoader = new THREE.STLLoader();
        // oStlLoader.load('models/elements/Art_Cube_.stl', callfunc);
        oStlLoader.load(name, callfunc);
        // oStlLoader.load('models/elements/Batman.stl', callfunc);
    }
};
function callfunc(geometry) {
    var material = new THREE.MeshNormalMaterial();
    var mesh = new THREE.Mesh(geometry, material);
    loadingSTL.objects.push(mesh);
    mesh.rotation.set(-Math.PI / 2, 0, Math.PI / 2);
    mesh.position.set(-10, 0, 10);
    mesh.scale.set(2, 2, 2);
    loadingSTL.scene.add(mesh);
};

// Animate the scene
function animate() {
    requestAnimationFrame(animate);
    render();
    update();
}

// Update controls
function update() {
    loadingSTL.controls.update(loadingSTL.clock.getDelta());
}

// Render the scene
function render() {
    if (loadingSTL.renderer) {
        loadingSTL.renderer.render(loadingSTL.scene, loadingSTL.camera);
    }
}

// Initialize this on page load
function initialize() {
    loadingSTL.init();
    animate();
}


if (window.addEventListener)
    window.addEventListener('load', initialize, false);
else if (window.attachEvent)
    window.attachEvent('onload', initialize);
else window.onload = initialize;
