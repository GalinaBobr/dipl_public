package command;

import entity.Storage;
import manager.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class ClearSceneCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.main");
        HttpSession session = request.getSession(true);
        Storage.clearList();
        session.removeAttribute("output");
        return page;
    }
}
