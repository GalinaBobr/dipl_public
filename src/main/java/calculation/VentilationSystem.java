package calculation;

import static calculation.ReferenceTable.DIAMETERS;
import static calculation.ReferenceTable.VOLUMETRIC_AIR_HEAT;

public class VentilationSystem {
    /**
     * Норма расхода воздуха на одного человека, м^3/ч
     */
    private static final double AIR_FLOW_RATE_PER_PERSON = 30;
    /**
     * Нормируемая кратность воздухообмена для производственного помещения
     */
    private static final int NORMALIZE_AIR_EXCHANGE_RATE = 5;
    /**
     * Рекомендуемое значение скорости в магистральном воздуховоде в промышленном помещении (6-11м/с)
     */
    private static final double AIR_SPEED_IN_DUCT = 6;
    /**
     * Hазность температур воздуха на выходе и входе калорифера,°С.
     */
    private static final double AIR_TEMP_DIFFERENCE = 44;
    /**
     * Стоимость электроэнергии BYN за 1кВт за период минимальных нагрузок (с 22.00 до 17.00)
     */
    private static final double DAY_RATE_ENERGY = 0.0708;
    /**
     * Стоимость электроэнергии BYN за 1кВт за период максимальных нагрузок (с 17.00 до 22.00)
     */
    private static final double NIGHT_RATE_ENERGY = 0.2027;
    /**
     * Дневной перепад температур за март-май.
     */
    private static final double TEMP_DAY_ = 6;
    /**
     * Ночной перепад температур за март-май.
     */
    private static final double TEMP_NIGHT = 12;
    private static final int monthDays = 30;
    private double airCapacity;
    private double ductArea;

    /**
     * Расчет производительности по воздуху
     *
     * @param personQty  Количество людей
     * @param roomSquare Площадь помещения
     * @param roomHeight Высота помещения
     * @return производительность по воздуху, м^3/ч
     */
    public double getAirCapacity(int personQty, double roomSquare, double roomHeight) {
        airCapacity = Math.max(getAirExchangeByPersonQty(personQty), getAirExchangeInMultiplicity(roomSquare, roomHeight));
        return airCapacity;
    }

    /**
     * Расчет воздухообмена по количеству людей
     *
     * @param personQty Количество людей
     * @return воздухообмен по количеству людей
     */
    private double getAirExchangeByPersonQty(int personQty) {
        return AIR_FLOW_RATE_PER_PERSON * personQty;
    }

    /**
     * Расчет воздухообмена по кратности
     *
     * @param roomSquare Площадь помещения
     * @param roomHeight Высота помещения
     * @return Воздухообмен по кратности
     */
    private double getAirExchangeInMultiplicity(double roomSquare, double roomHeight) {
        return NORMALIZE_AIR_EXCHANGE_RATE * roomHeight * roomSquare;
    }

    /**
     * Вычисление расчетной площади сечения воздуховода
     *
     * @return Расчетная площадь сечения воздуховода
     */
    public double getDuctSize() {
        ductArea = airCapacity * ReferenceTable.MATCHING_COEFFICIENT / AIR_SPEED_IN_DUCT;
        return ductArea;
    }

    public double getRecommendedDiameterByDuctArea() {
        double calculatedDiameter = Math.sqrt(ductArea * 400 / Math.PI);
        if (calculatedDiameter < DIAMETERS[0]) {
            return DIAMETERS[0];
        }
        for (int i = 1; i < DIAMETERS.length; i++) {
            if (DIAMETERS[i] > calculatedDiameter && DIAMETERS[i - 1] < calculatedDiameter) {
                return DIAMETERS[i];
            }
        }
        return DIAMETERS[DIAMETERS.length - 1];
    }

    public String getRecommendedDimensionsOfAirDistributor() {
        double diameter = getRecommendedDiameterByDuctArea();
        return ReferenceTable.AIR_DISTRIBUTORS_DIM.get(diameter);
    }

    /**
     * Расчет мощности калорифера
     *
     * @return мощность калорифера
     */
    public double getAirHeaterPower() {
        return AIR_TEMP_DIFFERENCE * airCapacity * VOLUMETRIC_AIR_HEAT / 1000;
    }

    /**
     * Расчет максимального тока, потребляемого калорифером
     *
     * @return ток
     */
    public double getMaxAmperage() {

        return getAirHeaterPower() / defineSupplyVoltage();
    }

    /**
     * Определение напряжения питания
     *
     * @return напряжение питания
     */
    public double defineSupplyVoltage() {
        double supplyVoltage = 220;
        if (getAirHeaterPower() > 5.0) {
            supplyVoltage = 660;
        }
        return supplyVoltage;
    }

    /**
     * Расчет потребляемой электроэнергии
     *
     * @return потребляемая электроэнергия
     */
    public double getEnergyConsumption() {
        return (TEMP_DAY_ * airCapacity * VOLUMETRIC_AIR_HEAT * DAY_RATE_ENERGY * 19 +
                TEMP_NIGHT * airCapacity * VOLUMETRIC_AIR_HEAT * NIGHT_RATE_ENERGY * 5) * monthDays/1000;

    }
}
